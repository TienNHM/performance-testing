import http from 'k6/http';
import { check } from "k6";
import { randomItem } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import { SharedArray } from "k6/data";
import * as constants from "../constant/constants.js";

export const options = {
    scenarios: {
        View: {
            executor: 'ramping-arrival-rate',
            stages: [
            { duration: '30s', target: 100 },
            { duration: '10m', target: 500 },
            { duration: '20s', target: 50 },
            ],
            preAllocatedVUs: 500, // how large the initial pool of VUs would be
            maxVUs: 500, // if the preAllocatedVUs are not enough, we can initialize more
            tags: { test_type: 'api' }, // different extra metric tags for this scenario
            env: { MY_CROC_ID: '2' }, // same function, different environment variables
            exec: 'View', // same function as the scenario above, but with different env vars
        },
        ViewListProfile: {
            executor: 'ramping-arrival-rate',
            stages: [
            { duration: '30s', target: 50 },
            { duration: '10m', target: 200 },
            { duration: '20s', target: 50 },
            ],
            preAllocatedVUs: 50, // how large the initial pool of VUs would be
            maxVUs: 200, // if the preAllocatedVUs are not enough, we can initialize more
            tags: { test_type: 'api' }, // different extra metric tags for this scenario
            env: { MY_CROC_ID: '2' }, // same function, different environment variables
            exec: 'ViewListProfile', // same function as the scenario above, but with different env vars
        },
        CreateExchangeTransaction: {
            executor: 'ramping-arrival-rate',
            stages: [
            { duration: '30s', target: 20 },
            { duration: '10m', target: 70 },
            { duration: '20s', target: 50 },
            ],
            preAllocatedVUs: 70, // how large the initial pool of VUs would be
            maxVUs: 70, // if the preAllocatedVUs are not enough, we can initialize more
            tags: { test_type: 'api' }, // different extra metric tags for this scenario
            env: { MY_CROC_ID: '2' }, // same function, different environment variables
            exec: 'CreateExchangeTransaction', // same function as the scenario above, but with different env vars
        },
        RetroactiveExchangeTransaction: {
            executor: 'ramping-arrival-rate',
            stages: [
            { duration: '30s', target: 20 },
            { duration: '10m', target: 70 },
            { duration: '20s', target: 50 },
            ],
            preAllocatedVUs: 70, // how large the initial pool of VUs would be
            maxVUs: 70, // if the preAllocatedVUs are not enough, we can initialize more
            tags: { test_type: 'api' }, // different extra metric tags for this scenario
            env: { MY_CROC_ID: '2' }, // same function, different environment variables
            exec: 'RetroactiveExchangeTransaction', // same function as the scenario above, but with different env vars
        },
        BookToken: {
            executor: 'ramping-arrival-rate',
            stages: [
            { duration: '30s', target: 20 },
            { duration: '10m', target: 70 },
            { duration: '20s', target: 50 },
            ],
            preAllocatedVUs: 70, // how large the initial pool of VUs would be
            maxVUs: 70, // if the preAllocatedVUs are not enough, we can initialize more
            tags: { test_type: 'api' }, // different extra metric tags for this scenario
            env: { MY_CROC_ID: '2' }, // same function, different environment variables
            exec: 'BookToken', // same function as the scenario above, but with different env vars
        },
        UseToken: {
            executor: 'ramping-arrival-rate',
            stages: [
            { duration: '30s', target: 20 },
            { duration: '10m', target: 50 },
            { duration: '20s', target: 20 },
            ],
            preAllocatedVUs: 50, // how large the initial pool of VUs would be
            maxVUs: 50, // if the preAllocatedVUs are not enough, we can initialize more
            tags: { test_type: 'api' }, // different extra metric tags for this scenario
            env: { MY_CROC_ID: '2' }, // same function, different environment variables
            exec: 'UseToken', // same function as the scenario above, but with different env vars
        }
    },
    discardResponseBodies: true,
    // thresholds: {
    //   // we can set different thresholds for the different scenarios because
    //   // of the extra metric tags we set!
    //   'http_req_duration{test_type:api}': ['p(95)<250', 'p(99)<350'],
    //   'http_req_duration{test_type:website}': ['p(99)<500'],
    //   // we can reference the scenario names as well
    //   'http_req_duration{scenario:my_api_test_2}': ['p(99)<300'],
    // },

};

const params = {
  headers: {
    accept: "application/json",
    TenantId: 35 // tenant Long Chau
  },
};

var memberCodes = open("../../data/uat-lc/lc_members_code.txt").split("\r\n");
const members = new SharedArray("LC Members", function () {
    return JSON.parse(open("../../data/uat-lc/lc_member_in_bu.json"));
});
const LENGTH = members.length;

let merchantLoyaltyIds = open("../../data/uat-lc/lc_merchantLoyaltyIds.txt").split("\r\n");

export function View() {
    var memberCode = randomItem(memberCodes);
    // console.log(memberCode);
    
    var url = "https://xxx.com/member-svc/services/app/Member/View?MemberCode=" + memberCode;
    var res = http.get(url, params);
    var t = check(res, {
        [`View: is status 200`]: (r) => r.status === 200
    });
}

export function ViewListProfile(data) {
    const length = constants.getRndInteger(1, 1);
    const listMerchantLoyaltyIds = [];
    for (let i = 0; i < length; i++) {
      var index = constants.getRndInteger(0, LENGTH);
      listMerchantLoyaltyIds.push(members[index].MerchantLoyaltyId);
    }

    //console.log(listMerchantLoyaltyIds);
    var url = `https://xxx.com/member-svc/services/app/Member/ViewListProfiles?MerchantLoyaltyId=${listMerchantLoyaltyIds.join(',')}`;
    var res = http.get(url, params);
    var t = check(res, {
        [`ViewProfile: is status 200`]: (r) => r.status === 200
    });
}

export function CreateUpdateMember(data) {
    var number = makeNumber(9);
    var phone = "0" + number;
    var date = new Date().toISOString().split('T')[0];
    const slug = "0819";

    var payload = JSON.stringify(
        {
            MerchantLoyaltyId: `loadtest_${slug}_${number}`,
            Phone: phone,
            FullName: `loadtest_${slug}_${number}`,
            Birthday: date,
            Gender: "M",
            Email: `loadtest.${slug}.${phone}@gmail.com`,
            Address: 'Cần Thơ',
        }
    );
    var url = "https://xxx.com/member-svc/services/app/Member/CreateUpdateMember";
    var res = http.post(url, payload, params);
    // console.log(JSON.stringify(res));
    var t = check(res, {
        [`$CreateUpdateMember: is status 200`]: (r) => r.status === 200
    });
}

export function CreateExchangeTransaction(data) {
    var url = "https://xxx.com/htl-transaction-svc/services/app/Transaction/CreateExchangeTransactionV2";

    const slug = '1908';
    var payload = JSON.stringify({
        memberNetworkID: '0000645b-cc7e-40be-a696-3e70d7321325',
        merchantTranstype: 'Referral',
        merchantLoyaltyId: 'loadtest_0806_675184188', // '53428202x202x26v80c4',
        rewardPoint: 1,
        rankingPoint: 1,
        merchantReason: `loadtest_${slug}`,
        transactionCode: `ex_${constants.makeNumber(5)}_${Date.now()}`,
        transactionDate: new Date().toISOString().split('T')[0],
        expiryDate: new Date().toISOString().split('T')[0],
    });

    let res = http.post(url, payload, params);
        //console.log(JSON.stringify(res));
    var t = check(res, {
        [`$CreateExchangeTransaction: is status 200`]: (r) => r.status === 200
    });
}

export function UseToken(data) {
    const slug = '1908';
    var url = "https://xxx.com/htl-transaction-svc/services/app/Transaction/UseToken";
    var merchantLoyaltyId = randomItem(merchantLoyaltyIds);

    var payload = JSON.stringify({
        productReleasedBy: 'self_produced',
        merchantTranstype: 'RedeemPartnerGift',
        merchantLoyaltyId: merchantLoyaltyId, //'q382a2l242e2a28do2av',
        rewardPoint: 0,
        merchantReason: `loadtest_${slug}`,
        transactionCode: `ut_${constants.makeNumber(5)}_${Date.now()}`,
        transactionDate: new Date().toISOString().split("T")[0],
    });

    let res = http.post(url, payload, params);
    //   console.log(JSON.stringify(res));
    // if (!res.body.includes('"success":true')) {
    //   console.log(JSON.stringify(res));
    // }

    var t = check(res, {
        [`$UseToken: is status 200`]: (r) => r.status === 200,
        //[`$UseToken: body contains success true`]: (r) => r.body.includes('"success":true'),
    });
}

export function RetroactiveExchangeTransaction(data) {
    var url = "https://xxx.com/htl-transaction-svc/services/app/Transaction/RetroactiveExchangeTransaction";

    const slug = '1908';
    var payload = JSON.stringify({
        memberNetworkID: '0000645b-cc7e-40be-a696-3e70d7321325',
        merchantLoyaltyId: 'loadtest_0806_675184188',
        merchantTranstype: 'Retroactive',
        merchantReason: `loadtest_${slug}`,
        transactionDate: new Date().toISOString().split('T')[0],
        expiryDate: new Date().toISOString().split('T')[0],
        rewardPoint: 1,
        rankingPoint: 1
    });

    let res = http.post(url, payload, params);
        //console.log(JSON.stringify(res));
    var t = check(res, {
        [`$RetroactiveExchangeTransaction: is status 200`]: (r) => r.status === 200
    });
}

export function BookToken(data) {
    var url = "https://xxx.com/htl-transaction-svc/services/app/BookingLogs/BookToken";

    var member = members[(__ITER * 500 + __VU) % LENGTH];
  
    var payload = JSON.stringify({
        merchantLoyaltyId: member.MerchantLoyaltyId,
        code: `lb_${constants.makeNumber(5)}_${Date.now()}`,
        status: "Book"
    });

    let res = http.post(url, payload, params);
        //console.log(JSON.stringify(res));
    var t = check(res, {
        [`$BookToken: is status 200`]: (r) => r.status === 200
    });
}