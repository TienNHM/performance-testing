/**
 * @name CreateRedeemTransactionVer2
 * @summary Create Redeem Transaction Ver2
 * @description docker-compose run k6 run /scripts//Gift/UpdateVoucherStatus.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow } from "../utils/DateTimeHelper.js";

var VUS = 200;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "5m",
  rps: VUS
};

const REDEEM_TX = new SharedArray("GiftRedeemTransaction", function () {
  return JSON.parse(open("../data/GiftRedeemTransaction3.json"));
});

const API = "EGiftInfors/UpdateVoucherStatus";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": CONTENT_TYPE.JSON,
    "Content-Type": CONTENT_TYPE.JSON,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6ImFhZTZhYzkwLTEzMzQtNGZmNC04MTQxLTU4YjdmYTM4MjAwZSIsImlhdCI6MTY4ODYwOTUyNywidG9rZW5fdmFsaWRpdHlfa2V5IjoiM2U5ZDkwMWQtYTM4NS00YjUyLWIxMDEtNDk1YzdjNjg5YjUzIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NjA5NTI3LCJleHAiOjE2ODg2OTU5MjcsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.5YZ814LROoSNFdThjhJKta9DuoEsYmVL6VhZFsimR5M"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  var redeemTx = REDEEM_TX[(__ITER * VUS + __VU) % REDEEM_TX.length];

  var payload = JSON.stringify(
    {
      "memberCode": redeemTx.OwnerCode,
      "giftCode": redeemTx.Producer,
      "eGiftCode": redeemTx.EGiftCode,
      "status": redeemTx.EGiftStatus
    }
  );
  let res = http.put(`${url}`, payload, params);

  if (!res.body || !res.body.includes('"success":true') || !res.body.includes('"code":711')) {
    console.log(JSON.stringify(res.body));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    // [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} check`]: (r) => res.body.includes('"success":true') || res.body.includes('"code":711'),
  });

  if (!success) {
    errorRate.add(1);
  }
};