# Perfomance Testing with K6 and InfluxDB / Grafana

## Dashboards
The dashboard in /dashboards is adapted from the excellent K6 / Grafana dashboard here:
https://grafana.com/grafana/dashboards/2587

There are only two small modifications:
* the data source is configured to use the docker created InfluxDB data source
* the time period is set to now-15m

## Installation
### Install Docker and Docker Compose
Follow the instructions here: https://docs.docker.com/get-docker/ and here: https://docs.docker.com/compose/install/

### Install K6
Follow the instructions here: https://k6.io/docs/getting-started/installation/

## Running with Docker Compose
### Start InfluxDB and Grafana
To start InfluxDB and Grafana, simply run the following command from the root of the project:
```bash
docker-compose up -d influxdb grafana
```

### Run the test
To run the test, simply run the following command from the root of the project:
```bash
docker-compose run k6 run /scripts/<path-to-test-file>
```

Example:
```bash
docker-compose run k6 run /scripts//Article/GetAllArticleAndRelatedNewsVer2_Mobile.js
```

### View the results
To view the dashboard, open a browser and navigate to [this link](http://localhost:3000/d/k6/k6-load-testing-results?orgId=1&refresh=5s).

## Running locally without Docker Compose (not recommended)
Run the following commands from the root of the project:
```bash
k6 run /scripts/<path-to-test-file>
```

Example:
```bash
k6 run /scripts//Article/GetAllArticleAndRelatedNewsVer2_Mobile.js
```

## References
- https://medium.com/swlh/beautiful-load-testing-with-k6-and-docker-compose-4454edb3a2e3