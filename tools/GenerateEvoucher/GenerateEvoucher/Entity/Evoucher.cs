﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateEvoucher.Entity
{
    public class Evoucher
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string UsedStatus { get; set; }
    }
}
