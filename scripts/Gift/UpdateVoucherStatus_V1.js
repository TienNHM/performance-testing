/**
 * @name UpdateVoucherStatus_V1
 * @summary Update voucher status v1
 * @description docker-compose run k6 run /scripts//Gift/UpdateVoucherStatus_V1.js --logformat raw --console-output=/scripts//logs/UpdateVoucherStatus_V1.log
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { scenario } from 'k6/execution';
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { Choice, RandomString } from "../utils/Random.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow } from "../utils/DateTimeHelper.js";

const REDEEM_TX = new SharedArray("eGifts", function () {
  return JSON.parse(open("../data/eGifts2.json"));
});

var VUS = 50;
// export let options = {
//   scenarios: {
//     'UpdateVoucherStatus_V1': {
//       executor: 'shared-iterations',
//       vus: VUS,
//       iterations: REDEEM_TX.length,
//       maxDuration: '2m',
//     },
//   },
// };

export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "2m",
  rps: VUS
};

const API = "EGiftInfors/UpdateVoucherStatus_V1";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": CONTENT_TYPE.JSON,
    "Content-Type": CONTENT_TYPE.JSON_PATCH,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjI3NjM1YzlmLTJkYzEtNGRjYi04OTg5LTg4MzNlMDQyMTM5OSIsImlhdCI6MTY4ODAzNjUwOCwidG9rZW5fdmFsaWRpdHlfa2V5IjoiODVkZTI0ODEtZTYwOS00YTVlLTlkNmQtODhiMTM3NGMzNzI0IiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4MDM2NTA4LCJleHAiOjE2ODgxMjI5MDgsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.zp7p-ExD9lwpJR231i_1suyIcxIFAuMSBlQ7O00kdtg"
  }
};

export function GetNextStatus(status) {
  switch (status) {
    case 'R': return Choice(['U', 'E', 'H']);
    case 'H': return Choice(['U', 'E', 'R']);
    case 'U': return 'R';
    default: return 'E';
  }
}

export function GetOrderCode(currentStatus, nextStatus, EGiftCode, OrderCode) {
  if (currentStatus === 'R') {
    return `LOADTEST_${EGiftCode}_${RandomString(4)}`;
  }

  return `${OrderCode}`;

  // if (currentStatus === 'H') {
  //   return `${OrderCode}`;
  // }

  // if (currentStatus === 'U') {
  //   return `${OrderCode}`;
  // }
  // return `${OrderCode}`;
}

export let errorRate = new Rate("errors");

export default function (data) {

  // var egift = REDEEM_TX[scenario.iterationInTest];
  var egift = REDEEM_TX[(__ITER * VUS + __VU) % REDEEM_TX.length];

  var status = GetNextStatus(egift.EGiftStatus);
  var orderCode = GetOrderCode(egift.EGiftStatus, status, egift.EGiftCode, egift.OrderCode);
  var payload = JSON.stringify(
    {
      "memberCode": egift.OwnerCode,
      "giftCode": egift.Producer,
      "eGiftCode": egift.EGiftCode,
      "status": status,
      "orderCode": orderCode || null
    }
  );
  let res = http.put(`${url}`, payload, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
  });

    if (!success) {
        errorRate.add(1);
    }
};