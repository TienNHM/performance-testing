﻿using GenerateEvoucher.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GenerateEvoucher.Helper.RandomHelper;

namespace GenerateEvoucher
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var prefix = txtPrefix.Text;
            var quantity = Convert.ToInt32(numQuantity.Text);
            var voucherLength = Convert.ToInt32(numLength.Text);
            var description = txtDescription.Text;

            var randomType = GetRandomType();

            var result = new List<Evoucher>();
            for (int i = 0; i < quantity; i++)
            {
                var code = Helper.RandomHelper.RandomString(voucherLength, prefix, randomType, ckbIsUpperCase.Checked);
                var egiftDescription = !string.IsNullOrWhiteSpace(description) ? $"{code}_{description}" : code;

                var evoucher = new Evoucher
                {
                    Code = code,
                    Description = egiftDescription,
                    Status = "Active",
                    UsedStatus = "New"
                };
                result.Add(evoucher);
            }

            dlgSaveFile.Filter = ".xlsx Files (*.xlsx)|*.xlsx";
            dlgSaveFile.DefaultExt = "xlsx";
            dlgSaveFile.OverwritePrompt = true;
            var dialogResult = dlgSaveFile.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                // Save to excel file using NPOI
                Helper.ExcelHelper.SaveToFile(dlgSaveFile.FileName, result, "EGift");
                MessageBox.Show("Done", "Generate Evoucher", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private RandomType GetRandomType()
        {
            var randomType = RandomType.Alphanumeric;
            if (btnAllChars.Checked)
                randomType = RandomType.AllChars;
            else if (btnAlphabet.Checked)
                randomType = RandomType.Alphabet;
            else if (btnNumbers.Checked)
                randomType = RandomType.Numbers;
            return randomType;
        }

        private void GenerateSampleEvoucher(object sender, EventArgs e)
        {
            var prefix = txtPrefix.Text;
            var voucherLength = Convert.ToInt32(numLength.Text);
            var description = txtDescription.Text;

            var randomType = GetRandomType();
            var code = Helper.RandomHelper.RandomString(voucherLength, prefix, randomType, ckbIsUpperCase.Checked);
            txtSample.Text = code;

            var egiftDescription = !string.IsNullOrWhiteSpace(description) ? $"{code}_{description}" : code;
            txtSampleDescription.Text = egiftDescription;
        }
    }
}
