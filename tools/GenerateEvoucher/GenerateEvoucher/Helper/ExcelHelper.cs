﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenerateEvoucher.Entity;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace GenerateEvoucher.Helper
{
    public static class ExcelHelper
    {
        // Save list of Evoucher to excel file using NPOI
        public static void SaveToFile(string filePath, List<Evoucher> evouchers, string sheetName)
        {
            var workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet(sheetName);

            var rowIndex = 0;
            var row = sheet.CreateRow(rowIndex);
            row.CreateCell(0).SetCellValue("Code");
            row.CreateCell(1).SetCellValue("Description");
            row.CreateCell(2).SetCellValue("Status");
            row.CreateCell(3).SetCellValue("Used status");

            foreach (var evoucher in evouchers)
            {
                rowIndex++;
                row = sheet.CreateRow(rowIndex);
                row.CreateCell(0).SetCellValue(evoucher.Code);
                row.CreateCell(1).SetCellValue(evoucher.Description);
                row.CreateCell(2).SetCellValue(evoucher.Status);
                row.CreateCell(3).SetCellValue(evoucher.UsedStatus);
            }

            // set auto width for all columns
            for (int i = 0; i < 4; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            using (var fileData = System.IO.File.Create(filePath))
            {
                workbook.Write(fileData);
            }
        }
    }
}
