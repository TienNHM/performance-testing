export const CONTENT_TYPE = {
    JSON: 'application/json',
    JSON_PATCH: 'application/json-patch+json',
    FORM: 'application/x-www-form-urlencoded'
}

export const MSL_MOBILE_URL = 'https://xxx.com'
export const MSL_LOYALTY_URL = 'https://xxx.com'

export const ACCOUNT = {
    QA: {
        EMAIL: 'xxx',
        PASSWORD: 'xxx'
    }
}