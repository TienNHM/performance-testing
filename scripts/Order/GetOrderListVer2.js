/**
 * @name GetOrderListVer2
 * @summary Get Order List Ver2
 * @description docker-compose run k6 run /scripts//Order/GetOrderListVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow } from "../utils/DateTimeHelper.js";
import { DateTimeNow } from "../utils/DateTimeHelper.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/accounts.json"));
});

const VUS = 50;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "5m",
  rps: VUS
};

const API = "Transactions/GetOrderListVer2";
var url = `${MSL_MOBILE_URL}/api/${API}`;
// var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": "text/plain",
    // "content-type": CONTENT_TYPE.JSON,
    "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjhkMDNhZTdmNDczZjJjNmIyNTI3NmMwNjM2MGViOTk4ODdlMjNhYTkiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbWFpc29uLXByb2QiLCJhdWQiOiJtYWlzb24tcHJvZCIsImF1dGhfdGltZSI6MTY4NzkyNTIyMCwidXNlcl9pZCI6Ild6aDZ5MFFPcExOMG9WRmh0b09LaVoxOGd5czEiLCJzdWIiOiJXemg2eTBRT3BMTjBvVkZodG9PS2laMThneXMxIiwiaWF0IjoxNjg3OTI1MjIwLCJleHAiOjE2ODc5Mjg4MjAsInBob25lX251bWJlciI6Iis4NDM4ODk2MzM0NSIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsicGhvbmUiOlsiKzg0Mzg4OTYzMzQ1Il19LCJzaWduX2luX3Byb3ZpZGVyIjoicGhvbmUifX0.iYbjCLDioujEXuXa0-puLuHOlA8qLJyLGF8kMKtkKDlCB8SYbjhYfXYA31wsnPz7L-vspIcldaOtXymWpGiTLpFpYNpEdX4I1vH611NUELp-jFQOHncReBv0s1WextUOf92kw9JzABGRZsH7U3IrS0Y--TGMeUIH0o7K9_iaAncrVjVITz6y8MIXAWmXK_BGCUdXgMbdXJ9IR6k32QTtLc-ZB5JbYLXMRufqy_TpFjprb-p8swixtCYMO-Gygv9DYkRalVT101XElUFl0xfQsj7MX6WAibG0cWUVKPKPc91jJir9ij38tCjyRI6aVP8Rp7VRN_OiQksEhAtDWnV9_g"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
  const urlParams = ConvertURLSearchParams({
    MemberCode: 'XXX164010784',
    SkipCount: 0,
    MaxResultCount: 1,
    OrderStatus: "Completed",
    OrderType: "Offline",
    FromDateFilter: '2023-06-27',
    ToDateFilter: '2023-06-28',
  });

  let res = http.get(`${url}?${urlParams}`, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
  });

    if (!success) {
        errorRate.add(1);
    }
};