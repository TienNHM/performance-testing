/**
 * @name UseCoin
 * @summary Use Coin
 * @description docker-compose run k6 run /scripts//Order/UseCoin.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { MSL_LOYALTY_URL, CONTENT_TYPE } from "../utils/Constants.js";
import { UtcNow } from "../utils/DateTimeHelper.js";
import { RandomCode } from "../utils/Random.js";

const MEMBERS = new SharedArray("members", function () {
    return JSON.parse(open("../data/accounts.json"));
});

const dataMemberCodes = open("../data/MemberCodes.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const VUS = 200;
export let options = {
    // stages: [
    //   {duration: "1m", target: 400},
    //   {duration: "5m", target: 400},
    // ]
    vus: VUS,
    duration: "5m",
    rps: VUS
};

const API = "Order/UseCoin";
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
    headers: {
        "Accept": CONTENT_TYPE.JSON,
        "Content-Type": CONTENT_TYPE.JSON_PATCH,
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjdkMmI3ZGU3LWRiZjAtNDZkOS05YzBlLTA2MzRiMGUwN2M5MyIsImlhdCI6MTY4ODQzNTAwNCwidG9rZW5fdmFsaWRpdHlfa2V5IjoiYmVmYjY2M2MtNzVlYS00YTE5LWI4Y2MtODI3OTNhNWM4MmJhIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NDM1MDA0LCJleHAiOjE2ODg1MjE0MDQsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.mWZWai5FaKG5qKiMFKj5u0ergf9-DvpOJdC6ODzugI4"
    }
};

export let errorRate = new Rate("errors");

export default function (data) {

    try {
        var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];

        var payload = JSON.stringify({
            "memberCode": memberCode,
            "orderCode": `LOADTEST_Use_${RandomCode()}`,
            "businessTime": `${UtcNow()}`,
            "totalRequestedAmount": 1
        });

        let res = http.post(`${url}`, payload, params);

        if (!res.body || !res.body.includes('"success":true')) {
            console.log(JSON.stringify(res));
            console.log(payload);
        }

        var success = check(res, {
            // "log": r => console.log(JSON.stringify(r)),
            [`${API} is status 200`]: (r) => res.status === 200,
            [`${API} body contains success true`]: (r) => res.body.includes('"success":true') // || res.body.includes('enough') || res.body.includes('Total amout not greater coin'),
        });

        if (!success) {
            errorRate.add(1);
        }
    }
    catch (error) {
        console.log(error);
        errorRate.add(1);
    }
};