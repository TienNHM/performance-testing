/**
 * @name PreCalculatePoint
 * @summary PreCalculate Point
 * @description docker-compose run k6 run /scripts//Order/PreCalculatePoint.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow } from "../utils/DateTimeHelper.js";
import { DateTimeNow } from "../utils/DateTimeHelper.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/members.json"));
});

const dataMemberCodes = open("../data/MemberCodes.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const VUS = 200;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "5m",
  rps: VUS
};

const API = "Orders/PreCalculatePoint";
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": "text/plain",
    "content-type": CONTENT_TYPE.JSON_PATCH,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjdkMmI3ZGU3LWRiZjAtNDZkOS05YzBlLTA2MzRiMGUwN2M5MyIsImlhdCI6MTY4ODQzNTAwNCwidG9rZW5fdmFsaWRpdHlfa2V5IjoiYmVmYjY2M2MtNzVlYS00YTE5LWI4Y2MtODI3OTNhNWM4MmJhIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NDM1MDA0LCJleHAiOjE2ODg1MjE0MDQsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.mWZWai5FaKG5qKiMFKj5u0ergf9-DvpOJdC6ODzugI4"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  try {
    // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
    var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];

    var payload = JSON.stringify({
      "tenantId": 3,
      "memberCode": memberCode,
      "standardMemberCode": memberCode,
      "reason": "Invoice",
      "businessTime": `${UtcNow()}`,
      "totalCalcAmount": 2500000,
      "rankTypeCode": "",
      "fullMemberTypeCode": "Customer",
      "fullRegionCode": "VN,MN,CT",
      "fullSalesRegionCode": "",
      "fullChannelTypeCode": "",
      "totalDetailLine": 1,
      "specialRewarding": false,
      "partnerTimeZone": 0,
      "ordersDetail": [
        {
          "seqNo": 1,
          "productCode": "PD0001",
          "fullDeptCode": "",
          "fullClassCode": "",
          "quantity": 1,
          "unitPrice": 1500000,
          "calcAmount": 1500000
        },
        {
          "seqNo": 2,
          "productCode": "PD0001",
          "fullDeptCode": "",
          "fullClassCode": "",
          "quantity": 2,
          "unitPrice": 500000,
          "calcAmount": 1000000
        }
      ],
      "orderChannel": "",
      "distributorCode": ""
    });

    let res = http.post(`${url}`, payload, params);

    if (!res.body || !res.body.includes('"success":true')) {
      console.log(JSON.stringify(res));
    }

    var success = check(res, {
      // "log": r => console.log(JSON.stringify(r)),
      [`${API} is status 200`]: (r) => res.status === 200,
      [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
    });

    if (!success) {
      errorRate.add(1);
    }
  }
  catch (error) {
    console.log(error);
    errorRate.add(1);
  }
};