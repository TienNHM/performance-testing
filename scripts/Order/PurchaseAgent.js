/**
 * @name PurchaseAgent
 * @summary Purchase Agent
 * @description docker-compose run k6 run /scripts//Order/PurchaseAgent.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { scenario } from 'k6/execution';
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow } from "../utils/DateTimeHelper.js";
import { DateTimeNow } from "../utils/DateTimeHelper.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/members.json"));
});

const dataMemberCodes = open("../data/MemberCodes.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const VUS = 200;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]

  vus: VUS,
  duration: "5m",
  rps: VUS

  // scenarios: {
  //   'PurchaseAgent': {
  //     executor: 'shared-iterations',
  //     vus: VUS,
  //     iterations: MEMBER_CODES.length,
  //     maxDuration: '5m',
  //   },
  // },
};

const API = "Order/PurchaseAgent";
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "content-type": CONTENT_TYPE.JSON,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjYyNWJjYjE4LTJiMzAtNDhiMS04YWU5LWUyZjhiNTI5YTdkYiIsImlhdCI6MTY4ODQ1MjM4MiwidG9rZW5fdmFsaWRpdHlfa2V5IjoiOGM1NWJhZmEtN2NhOC00ZWNmLWFiODUtZGI2ZjIxMTcyMGI4IiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NDUyMzgyLCJleHAiOjE2ODg1Mzg3ODIsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.aJ81ygSmI67zPBKKTCjK43TpuCW3u8n5eRgfCWF6iqI"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  var offset = __ITER * VUS + __VU;
  // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
  var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];
  // var memberCode = MEMBER_CODES[scenario.iterationInTest];

  var payload = JSON.stringify({
    "member": {
      "memberCode": memberCode,
      "standardMemberCode": memberCode,
      "rankTypeCode": "Customer",  //Customer ; Employee
      "fullMemberTypeCode": "Customer",
      "fullRegionCode": "VN,MN,CT",
      "fullSalesRegionCode": "",
      "fullChannelTypeCode": "",
      "mlmDealerChannel": "",
      "mlmDistributionChannel": "",
      "mlmApplied": true,
      "specialRewarding": false
    },
    "order": {
      "tenantId": 3,
      "reason": "Invoice",
      "businessTime": `${UtcNow()}`,
      "originalOrders": "",
      "orderCode": `LOADTEST_${DateTimeNow()}_${offset}`,
      "totalAmountIncludeTax": 0,
      "totalAmountBeforeTax": 0,
      "totalCalcAmount": 1500000,
      "totalTaxAmount": 0,
      "totalRedemptionAmount": 0,
      "totalPromotionDiscount": 0,
      "source": "",
      "totalDetailLine": 1,
      "sourceType": "",
      "totalLoyaltyDiscount": 0,
      "distributorCode": "4517", //data tạo từ Master data - Stores (Distributors)
      "fullDistributionChannelCode": "",
      "fullRegionCode": "",
      "description": "LOADTEST", //note trong view receipt
      "brandName": "LOADTEST", //Product Brand
      "paymentMethod": "COD",
      "partnerTimeZone": 0,
      "orderChannel": "Offline" //Để nhận thông báo Order Offline/Online. Để null thì offline
    },
    "ordersDetail": [
      {
        "seqNo": 1,
        "originalOrderLine": 0,
        "productCode": "LOADTEST",
        "fullDeptCode": "LOADTEST", // vendor
        "fullClassCode": "IP",
        "quantity": 1,
        "unitPrice": 1500000,
        "amountBeforeTax": 0,
        "amountIncludeTax": 0,
        "taxAmount": 0,
        "calcAmount": 1500000,
        "hotDeal": false,
        "originalProductCode": "",
        "beforeDiscount": 0,
        "discount": 0,
        "loyaltyDiscount": 0,
        "redemptionAmount": 0,
        "promotionDiscount": 0,
        "allowIncentive": true
      }
    ]
  });

  let res = http.post(`${url}`, payload, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
  });

  if (!success) {
    errorRate.add(1);
  }
};