/**
 * @name JoinVer2
 * @summary Join quest ver2
 * @description docker-compose run k6 run /scripts//Quest/JoinVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { scenario } from "k6/execution";
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/members.json"));
});

const dataMemberCodes = open("../data/MemberCodes.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const QUESTS = new SharedArray("quests", function () {
  return JSON.parse(open("../data/quests.json"));
});

var VUS = 200;
export let options = {
  // scenarios: {
  //   'JoinVer2': {
  //     executor: 'shared-iterations',
  //     vus: VUS,
  //     iterations: MEMBERS.length,
  //     maxDuration: '0.1m',
  //   },
  // },

  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]

  vus: VUS,
  duration: "5m",
  rps: VUS
};

const API = "Quest/JoinVer2";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": "text/plain",
    "Content-Type": CONTENT_TYPE.JSON,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjdkMmI3ZGU3LWRiZjAtNDZkOS05YzBlLTA2MzRiMGUwN2M5MyIsImlhdCI6MTY4ODQzNTAwNCwidG9rZW5fdmFsaWRpdHlfa2V5IjoiYmVmYjY2M2MtNzVlYS00YTE5LWI4Y2MtODI3OTNhNWM4MmJhIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NDM1MDA0LCJleHAiOjE2ODg1MjE0MDQsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.mWZWai5FaKG5qKiMFKj5u0ergf9-DvpOJdC6ODzugI4"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  // var member = MEMBERS[scenario.iterationInTest];
  // var quest = QUESTS[scenario.iterationInTest];

  // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
  var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];
  var quest = QUESTS[(__ITER * VUS + __VU) % QUESTS.length];

  var payload = JSON.stringify({
    memberCode: memberCode,
    questCode: quest.quest.code
  });

  let res = http.post(`${url}`, payload, params);

  if (!res.body || (!res.body.includes('"success":true') && !res.body.includes("You've already joined this challenge"))) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    // [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} Join successfully || already joined `]: (r) => res.body.includes('"success":true') || res.body.includes("You've already joined this challenge"),
  });

  if (!success) {
    errorRate.add(1);
  }
};