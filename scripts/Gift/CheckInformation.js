/**
 * @name CheckInformation
 * @summary Check information
 * @description docker-compose run k6 run /scripts//Gift/CheckInformation.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { scenario } from 'k6/execution';
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { Choice, RandomString } from "../utils/Random.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow } from "../utils/DateTimeHelper.js";

const REDEEM_TX = new SharedArray("eGifts", function () {
  return JSON.parse(open("../data/eGifts2.json"));
});

var VUS = 200;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "5m",
  rps: VUS
};

const API = "GiftInfors/CheckInformation";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": "text/plain",
    "Content-Type": CONTENT_TYPE.JSON_PATCH,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjZhMjVkMTViLWUyMmQtNDgwZS05NWYyLWVlOTA4YjBiNmI0ZCIsImlhdCI6MTY4Nzg1MDc4NSwidG9rZW5fdmFsaWRpdHlfa2V5IjoiM2RjZDIyOTEtZjcwNi00N2YyLTllZDktY2ViZTBiMzNjNjYxIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg3ODUwNzg1LCJleHAiOjE2ODc5MzcxODUsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.BWmTBS320ml4BDOUy36Y88mGJ7tmr41ll8_p1U8u4G0"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  // var egift = REDEEM_TX[scenario.iterationInTest];
  var egift = REDEEM_TX[(__ITER * VUS + __VU) % REDEEM_TX.length];

  var payload = JSON.stringify(
    {
      "memberCode": egift.OwnerCode,
      "eGiftCode": egift.EGiftCode,
    }
  );
  let res = http.post(`${url}`, payload, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} check`]: (r) => res.body.includes('"success":true') // || res.body.includes('Gift is redeeming') || res.body.includes('Not enough coin'),
  });

    if (!success) {
        errorRate.add(1);
    }
};