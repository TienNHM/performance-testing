/**
 * @name InputActionVer2
 * @summary Get rewards input action ver2
 * @description docker-compose run k6 run /scripts//Rewards/InputActionVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { RandomCode, RandomDate, RandomInt } from "../utils/Random.js";
import { UtcNow } from "../utils/DateTimeHelper.js";

const MEMBERS = new SharedArray("members", function () {
    return JSON.parse(open("../data/accounts.json"));
});

const dataMemberCodes = open("../data/MemberCodes_2.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const VUS = 200;
export let options = {
    // stages: [
    //   {duration: "1m", target: 400},
    //   {duration: "5m", target: 400},
    // ]
    vus: VUS,
    duration: "5m",
    rps: VUS
};

const API = "Rewards/InputActionVer2";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
    headers: {
        "accept": "text/plain",
        "content-type": CONTENT_TYPE.JSON,
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjU0NWFmYmQ3LTA1MTUtNDMzYi05OTJhLWM4NDc3ZGI3MmM0NSIsImlhdCI6MTY4ODQ2OTY0MywidG9rZW5fdmFsaWRpdHlfa2V5IjoiNzdmZDllYmUtYThiNC00NGVkLTlmMWQtNjdkYjU1ZjA3NjJiIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NDY5NjQzLCJleHAiOjE2ODg1NTYwNDMsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.xPz4Ng2e8BjEM4XHLwodIrlbEYzNkRk0n23D48Yr_AE"
    }
};

export let errorRate = new Rate("errors");

export default function (data) {

    var startDate = new Date();
    var endDate = new Date();
    endDate.setDate(endDate.getDate() + RandomInt(1, 365));

    // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
    var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];

    var payload = JSON.stringify({
        "actions": [
            {
                "transactionCode": `LOADTEST_${RandomCode()}`,
                "type": 1,  //1: tích, 2: ? , 3: Return và truyền Original
                "originalTransactionCode": "",
                "actionCode": "ReadNews", //CheckInEvent ; ReadNews; DailyLogin; FirstLogin;
                "actionFilter": "",
                "memberCode": memberCode,
                "value": 1,
                "actionTime": `${RandomDate(startDate, endDate)}`,
                "tag": "",
                "referenceCode": ""
            }
        ]
    });

    let res = http.post(`${url}`, payload, params);

    if (!res.body || !res.body.includes('"success":true')) {
        console.log(JSON.stringify(res));
    }

    var success = check(res, {
        // "log": r => console.log(JSON.stringify(r)),
        [`${API} is status 200`]: (r) => res.status === 200,
        [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
    });

    if (!success) {
        errorRate.add(1);
    }
};