﻿namespace GenerateEvoucher
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.numQuantity = new System.Windows.Forms.NumericUpDown();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.numLength = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAllChars = new System.Windows.Forms.RadioButton();
            this.btnAlphaNumeric = new System.Windows.Forms.RadioButton();
            this.btnNumbers = new System.Windows.Forms.RadioButton();
            this.btnAlphabet = new System.Windows.Forms.RadioButton();
            this.ckbIsUpperCase = new System.Windows.Forms.CheckBox();
            this.grpGeneralInfo = new System.Windows.Forms.GroupBox();
            this.grpSample = new System.Windows.Forms.GroupBox();
            this.txtSample = new System.Windows.Forms.TextBox();
            this.txtSampleDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLength)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grpGeneralInfo.SuspendLayout();
            this.grpSample.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(476, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "GENERATE EVOUCHER";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numQuantity
            // 
            this.numQuantity.Location = new System.Drawing.Point(292, 59);
            this.numQuantity.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numQuantity.Name = "numQuantity";
            this.numQuantity.Size = new System.Drawing.Size(120, 20);
            this.numQuantity.TabIndex = 2;
            this.numQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(68, 60);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(195, 20);
            this.txtPrefix.TabIndex = 1;
            this.txtPrefix.TextChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnGenerate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnGenerate.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.Location = new System.Drawing.Point(0, 442);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(476, 46);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // numLength
            // 
            this.numLength.Location = new System.Drawing.Point(292, 111);
            this.numLength.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLength.Name = "numLength";
            this.numLength.Size = new System.Drawing.Size(120, 20);
            this.numLength.TabIndex = 4;
            this.numLength.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLength.ValueChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Prefix";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Quantity";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(289, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Length";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(68, 111);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(195, 20);
            this.txtDescription.TabIndex = 8;
            this.txtDescription.TextChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Description";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.btnAllChars);
            this.groupBox1.Controls.Add(this.btnAlphaNumeric);
            this.groupBox1.Controls.Add(this.btnNumbers);
            this.groupBox1.Controls.Add(this.btnAlphabet);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 337);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(476, 105);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Loại evoucher";
            // 
            // btnAllChars
            // 
            this.btnAllChars.AutoSize = true;
            this.btnAllChars.Location = new System.Drawing.Point(271, 69);
            this.btnAllChars.Name = "btnAllChars";
            this.btnAllChars.Size = new System.Drawing.Size(151, 17);
            this.btnAllChars.TabIndex = 3;
            this.btnAllChars.Text = "Tất cả ký tự (ABC123@#!)";
            this.btnAllChars.UseVisualStyleBackColor = true;
            this.btnAllChars.CheckedChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // btnAlphaNumeric
            // 
            this.btnAlphaNumeric.AutoSize = true;
            this.btnAlphaNumeric.Checked = true;
            this.btnAlphaNumeric.Location = new System.Drawing.Point(47, 69);
            this.btnAlphaNumeric.Name = "btnAlphaNumeric";
            this.btnAlphaNumeric.Size = new System.Drawing.Size(186, 17);
            this.btnAlphaNumeric.TabIndex = 2;
            this.btnAlphaNumeric.TabStop = true;
            this.btnAlphaNumeric.Text = "Chứa chữ cái và chữ số (ABC123)";
            this.btnAlphaNumeric.UseVisualStyleBackColor = true;
            this.btnAlphaNumeric.CheckedChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // btnNumbers
            // 
            this.btnNumbers.AutoSize = true;
            this.btnNumbers.Location = new System.Drawing.Point(271, 33);
            this.btnNumbers.Name = "btnNumbers";
            this.btnNumbers.Size = new System.Drawing.Size(129, 17);
            this.btnNumbers.TabIndex = 1;
            this.btnNumbers.Text = "Chỉ chứa chữ số (123)";
            this.btnNumbers.UseVisualStyleBackColor = true;
            this.btnNumbers.CheckedChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // btnAlphabet
            // 
            this.btnAlphabet.AutoSize = true;
            this.btnAlphabet.Location = new System.Drawing.Point(47, 33);
            this.btnAlphabet.Name = "btnAlphabet";
            this.btnAlphabet.Size = new System.Drawing.Size(135, 17);
            this.btnAlphabet.TabIndex = 0;
            this.btnAlphabet.Text = "Chỉ chứa chữ cái (ABC)";
            this.btnAlphabet.UseVisualStyleBackColor = true;
            this.btnAlphabet.CheckedChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // ckbIsUpperCase
            // 
            this.ckbIsUpperCase.AutoSize = true;
            this.ckbIsUpperCase.Location = new System.Drawing.Point(68, 156);
            this.ckbIsUpperCase.Name = "ckbIsUpperCase";
            this.ckbIsUpperCase.Size = new System.Drawing.Size(128, 17);
            this.ckbIsUpperCase.TabIndex = 11;
            this.ckbIsUpperCase.Text = "Dùng kiểu CAPLOOK";
            this.ckbIsUpperCase.UseVisualStyleBackColor = true;
            this.ckbIsUpperCase.CheckedChanged += new System.EventHandler(this.GenerateSampleEvoucher);
            // 
            // grpGeneralInfo
            // 
            this.grpGeneralInfo.AutoSize = true;
            this.grpGeneralInfo.Controls.Add(this.numLength);
            this.grpGeneralInfo.Controls.Add(this.ckbIsUpperCase);
            this.grpGeneralInfo.Controls.Add(this.txtPrefix);
            this.grpGeneralInfo.Controls.Add(this.numQuantity);
            this.grpGeneralInfo.Controls.Add(this.label5);
            this.grpGeneralInfo.Controls.Add(this.label2);
            this.grpGeneralInfo.Controls.Add(this.txtDescription);
            this.grpGeneralInfo.Controls.Add(this.label3);
            this.grpGeneralInfo.Controls.Add(this.label4);
            this.grpGeneralInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpGeneralInfo.Location = new System.Drawing.Point(0, 29);
            this.grpGeneralInfo.Name = "grpGeneralInfo";
            this.grpGeneralInfo.Size = new System.Drawing.Size(476, 192);
            this.grpGeneralInfo.TabIndex = 12;
            this.grpGeneralInfo.TabStop = false;
            this.grpGeneralInfo.Text = "Thông tin chung";
            // 
            // grpSample
            // 
            this.grpSample.AutoSize = true;
            this.grpSample.Controls.Add(this.label7);
            this.grpSample.Controls.Add(this.label6);
            this.grpSample.Controls.Add(this.txtSampleDescription);
            this.grpSample.Controls.Add(this.txtSample);
            this.grpSample.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpSample.Location = new System.Drawing.Point(0, 211);
            this.grpSample.Name = "grpSample";
            this.grpSample.Size = new System.Drawing.Size(476, 126);
            this.grpSample.TabIndex = 13;
            this.grpSample.TabStop = false;
            this.grpSample.Text = "Mẫu";
            // 
            // txtSample
            // 
            this.txtSample.Location = new System.Drawing.Point(68, 32);
            this.txtSample.Name = "txtSample";
            this.txtSample.ReadOnly = true;
            this.txtSample.Size = new System.Drawing.Size(344, 20);
            this.txtSample.TabIndex = 0;
            // 
            // txtSampleDescription
            // 
            this.txtSampleDescription.Location = new System.Drawing.Point(68, 87);
            this.txtSampleDescription.Name = "txtSampleDescription";
            this.txtSampleDescription.ReadOnly = true;
            this.txtSampleDescription.Size = new System.Drawing.Size(344, 20);
            this.txtSampleDescription.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(65, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Description";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Evoucher code";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 488);
            this.Controls.Add(this.grpSample);
            this.Controls.Add(this.grpGeneralInfo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generate Evoucher";
            ((System.ComponentModel.ISupportInitialize)(this.numQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLength)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpGeneralInfo.ResumeLayout(false);
            this.grpGeneralInfo.PerformLayout();
            this.grpSample.ResumeLayout(false);
            this.grpSample.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numQuantity;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.NumericUpDown numLength;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.SaveFileDialog dlgSaveFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton btnNumbers;
        private System.Windows.Forms.RadioButton btnAlphabet;
        private System.Windows.Forms.RadioButton btnAlphaNumeric;
        private System.Windows.Forms.RadioButton btnAllChars;
        private System.Windows.Forms.CheckBox ckbIsUpperCase;
        private System.Windows.Forms.GroupBox grpGeneralInfo;
        private System.Windows.Forms.GroupBox grpSample;
        private System.Windows.Forms.TextBox txtSample;
        private System.Windows.Forms.TextBox txtSampleDescription;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
    }
}

