/**
 * @name GetByIdAndRelatedGiftVer2
 * @summary Gift infor Get By Id And Related Gift Ver2
 * @description docker-compose run k6 run /scripts//Gift/GetByIdAndRelatedGiftVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/accounts.json"));
});
const GIFTS = new SharedArray("gifts", function () {
  return JSON.parse(open("../data/listGifts.json"));
});

const VUS = 50;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "5m",
  rps: VUS
};

const API = "GiftInfos/GetByIdAndRelatedGiftVer2";
var url = `${MSL_MOBILE_URL}/api/${API}`;
// const API = "GiftInfors/GetByIdAndRelatedGiftVer2";
// var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": "text/plain",
    "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjhkMDNhZTdmNDczZjJjNmIyNTI3NmMwNjM2MGViOTk4ODdlMjNhYTkiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbWFpc29uLXByb2QiLCJhdWQiOiJtYWlzb24tcHJvZCIsImF1dGhfdGltZSI6MTY4Nzg1MDEzMiwidXNlcl9pZCI6Ild6aDZ5MFFPcExOMG9WRmh0b09LaVoxOGd5czEiLCJzdWIiOiJXemg2eTBRT3BMTjBvVkZodG9PS2laMThneXMxIiwiaWF0IjoxNjg3ODUwMTMyLCJleHAiOjE2ODc4NTM3MzIsInBob25lX251bWJlciI6Iis4NDM4ODk2MzM0NSIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsicGhvbmUiOlsiKzg0Mzg4OTYzMzQ1Il19LCJzaWduX2luX3Byb3ZpZGVyIjoicGhvbmUifX0.tCJ2umY770ROdozBrTrGIjLRIj4Xj-fUpQY6yohiR5OcOrna6ZPxC0FtVWs-PCGOYLExyEeCY00Gk_EGC6FDPB1dqEsnzOCUmtYudA2kw3JMHYavcddlpaoX2EDf2CoDn7T1pA13gEoZ_fVxvXDrCRIj8WDql4mW31H31-IJK_0uWYY3xkgxJo1kJA_yF5TszjJymqLkVaEzkVTbrcnc4WipecY1nMjnQiuFTUyLqf5ty6tLMvjSnnqSp0hHlc1fngdJpM77YD58n_HmgSTz6IjtZnh1aB1H-vt6GEQJECbyp6Yu-dLPxq1IxTYCsunO1C1vKUwGgw-4icLBwhA5cA"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
  var gift = GIFTS[(__ITER * VUS + __VU) % GIFTS.length];
  const urlParams = ConvertURLSearchParams({
    // "MemberCode": member.Code,
    "GiftId": gift.giftInfor.id,
    "MaxItem": 10
  });
  let res = http.get(`${url}?${urlParams}`, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
  });

    if (!success) {
        errorRate.add(1);
    }
};