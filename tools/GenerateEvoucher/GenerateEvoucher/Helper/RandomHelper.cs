﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateEvoucher.Helper
{
    public static class RandomHelper
    {
        private const string alphabet = "abcdefghijklmnopqrstuvwxyz";
        private const string numbers = "0123456789";
        private const string alphanumeric = "abcdefghijklmnopqrstuvwxyz0123456789";
        private const string allChars = "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|:<>?/.,;'[]-=~`";

        private static Random random = new Random();

        public enum RandomType
        {
            Alphabet,
            Numbers,
            Alphanumeric,
            AllChars
        }

        /// <summary>
        /// Generate random string with specific length and type of characters (alphabet, numbers, alphanumeric, all characters) and case (lower or upper)
        /// </summary>
        /// <param name="length"></param>
        /// <param name="randomType"></param>
        /// <param name="isLowerCase"></param>
        /// <returns></returns>
        public static string RandomString(int length, string prefix = "", RandomType randomType = RandomType.Alphanumeric, bool isUpperCase = false)
        {
            var chars = string.Empty;
            switch (randomType)
            {
                case RandomType.Alphabet:
                    chars = alphabet;
                    break;
                case RandomType.Numbers:
                    chars = numbers;
                    break;
                case RandomType.Alphanumeric:
                    chars = alphanumeric;
                    break;
                case RandomType.AllChars:
                    chars = allChars;
                    break;
            }

            var result = new string(Enumerable.Repeat(chars, length)
                             .Select(s => s[random.Next(s.Length)]).ToArray());

            result = $"{prefix}_{result}";

            return isUpperCase ? result.ToUpper() : result;
        }
    }
}
