/**
 * @name GetArticleByIdByMemberCodeVer2
 * @summary Get Article By Id By Member Code Ver2
 * @description docker-compose run k6 run /scripts//Article/GetArticleByIdByMemberCodeVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/accounts.json"));
});

const dataMemberCodes = open("../data/MemberCodes_2.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const ARTICLES = new SharedArray("articles", function () {
  return JSON.parse(open("../data/articles.json"));
});

const VUS = 200;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]

  vus: VUS,
  duration: "5m",
  rps: VUS
};

const API = "Article/GetArticleByIdByMemberCodeVer2";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
  headers: {
    "accept": "text/plain",
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6IjJmNjcwZGI4LWEyMzktNGJmZi1iNmZmLWM2YWUwY2I5NTVmMCIsImlhdCI6MTY4ODExODAxOCwidG9rZW5fdmFsaWRpdHlfa2V5IjoiOTg5NDU5MjctZjEyNi00MDdlLWEyYjctMTZjMTNjYjA1MmVkIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4MTE4MDE4LCJleHAiOjE2ODgyMDQ0MTgsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.scRn-zhbGY1ZsF8VGv4E6L0uxH7fFUcMq5yhoKstDmk"
  }
};

export let errorRate = new Rate("errors");

export default function (data) {

  // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
  var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];
  var articleItem = ARTICLES[(__ITER * VUS + __VU) % ARTICLES.length];

  const urlParams = ConvertURLSearchParams({
    "MemberCode": memberCode,
    "ArticleCode": articleItem.article.code
  });
  let res = http.get(`${url}?${urlParams}`, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => r.status === 200,
    [`${API} body contains success true`]: (r) => r.body.includes('"success":true')
  });

    if (!success) {
        errorRate.add(1);
    }
};