var response = {
    "result": {
        "totalCount": 17,
        "items": [
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 127,
                "giftCode": "GiftInfor_20230626101317110_8303",
                "giftName": "LOADTEST_LOADTEST",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/db09f865e61b6da8862d95aeadd7b5f2.png",
                "isInWishlist": false,
                "countGift": 12.000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_9TRNJNE428",
                    "LOADTEST_9Z6O4FGSLD",
                    "LOADTEST_VWX2UJIVYF",
                    "LOADTEST_13FALPGTOY",
                    "LOADTEST_7QW4VN3YSM",
                    "LOADTEST_YS3PMTSO9W",
                    "LOADTEST_8TDG804HQP",
                    "LOADTEST_ZFSNYNX9C4",
                    "LOADTEST_2IR5O2CV1D",
                    "LOADTEST_3PDDZ8Z1J0",
                    "LOADTEST_LVXAEMILFU",
                    "LOADTEST_YLTNWM1PLP"
                ],
                "giftTransactionCodes": [
                    "FC6597DB3CF8BD4F0B718498E78121F9",
                    "C5CD11994928A7A5AA23840697858424",
                    "AA49922AB9806BB9ADD7B93B2004756E",
                    "8ABDD8CBAE4BAFBC80D9361338D3D282",
                    "B54B03F3180A26F53AF6C9CF861A9EBE",
                    "D6826D8ECFF6BDB0F14A46FD7534C14C",
                    "0FD3FBC8BCF803F7698A73A4C365844B",
                    "EE8DAD6D8094E6E362600F061094E899",
                    "76D112F8108AB030D27E00246F9500E6",
                    "54E221DBF9175E18250CD43B649E335B",
                    "FD5245CB3BB62DE197CC7C01C8B07C3C",
                    "AE003635410C19C5DF7D4B42E3648159"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 120,
                "giftCode": "GiftInfor_20230626094701154_2854",
                "giftName": "LOADTEST_GIFT",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": null,
                "isInWishlist": false,
                "countGift": 6.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_PI2I9Q7N4O",
                    "LOADTEST_PM0MGGQG49",
                    "LOADTEST_UNEBTWVFSF",
                    "LOADTEST_6TBII80OQ3",
                    "LOADTEST_2ILOLJ01KJ",
                    "LOADTEST_DO1QXEKINC"
                ],
                "giftTransactionCodes": [
                    "EA6B590E8FA7CCE2874E1D6E8B95A4D5",
                    "221F5BB2DA3FEB4E75A156C1F86847A3",
                    "8742B4650100DAE94B0ECD79F2817E09",
                    "AF9293A76225E26E6454AE5517569FB1",
                    "8AEC95206694190AF04E45475748A7BE",
                    "035C9B715C0872A63FFD484E2276171A"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 131,
                "giftCode": "GiftInfor_20230627041051187_2510",
                "giftName": "LOADTEST_OOO",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/1c39c00178810dee4f32b79992055161.jpg",
                "isInWishlist": false,
                "countGift": 18.000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_48EPJDX04Q",
                    "LOADTEST_9Z6O4FGSLD",
                    "LOADTEST_VWX2UJIVYF",
                    "LOADTEST_Q4FKF7MEHA",
                    "LOADTEST_YBWVO8GTU5",
                    "LOADTEST_13FALPGTOY",
                    "LOADTEST_FL4N3JN6XH",
                    "LOADTEST_IU2Z2UANBW",
                    "LOADTEST_6LOHY1OQO1",
                    "LOADTEST_GVRU01OCR9",
                    "LOADTEST_7QW4VN3YSM",
                    "LOADTEST_1KBTFYHD0L",
                    "LOADTEST_YS3PMTSO9W",
                    "LOADTEST_8TDG804HQP",
                    "LOADTEST_ZFSNYNX9C4",
                    "LOADTEST_2IR5O2CV1D",
                    "LOADTEST_AWMLD24SG6",
                    "LOADTEST_AB0YBUGDV1"
                ],
                "giftTransactionCodes": [
                    "686EBC16FC436C3CB1CEED80F561555B",
                    "A40F3D80E14A3015BF11E9AE7A8F4B63",
                    "416CE1027CD934FCD525EACF2F85AF3B",
                    "CA1F591FC078501BC71EEC8DF7472D15",
                    "D1FB2B7C5D8FDA9A84F69B2C7F8EF996",
                    "DEB21A2F0E5B69CC248F06313B692511",
                    "80FAC279E85A22B875EEEA2E57DD51CF",
                    "2780C4A42F41B5335A90350A19C64BEA",
                    "1CB5C9382B003648E8ABD9F91509C8B6",
                    "A780C9D53E555048CE7FCA2B46325728",
                    "D5A70618E6FCADFB2F404F1678D8A208",
                    "1F39475306743F09B8F7FD856B2EC5A8",
                    "D8B5310082B25AD3B10A514680DAD15E",
                    "52F9E928D96E2B9E636CC65649EF0187",
                    "EBFE174599478CFBD1B894A5D4512D5E",
                    "FEB4768B4F689491D63C2AA41FEC15C0",
                    "7BE70C0F664C6C23C78AA92429B87E69",
                    "E1C2B860662B62F682BD1FD553C44965"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 119,
                "giftCode": "GiftInfor_20230626094535673_7330",
                "giftName": "LOADTEST_202306",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": null,
                "isInWishlist": false,
                "countGift": 13.000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_FLP35D1B01",
                    "LOADTEST_9Z1FPYVX78",
                    "LOADTEST_0IDGOA7BOI",
                    "LOADTEST_WI1JOJ4IUJ",
                    "LOADTEST_Q4ZRC59N2D",
                    "LOADTEST_B902KW2BDR",
                    "LOADTEST_SAVGDJKO7N",
                    "LOADTEST_BJS2E0FAGD",
                    "LOADTEST_NCPQLWH1RC",
                    "LOADTEST_YL98KCPIDV",
                    "LOADTEST_6TBII80OQ3",
                    "LOADTEST_DO1QXEKINC",
                    "LOADTEST_CL331ETI6B"
                ],
                "giftTransactionCodes": [
                    "792D0C4A430F7FC5703ED7F0E9A6CF7B",
                    "EFC813B5A7824792798295C4B3044DE1",
                    "65AECF9CD725DF7444EADB4E4D38CFAD",
                    "120F0C03372F0C98F4089273AA082657",
                    "0F863CC535D99B002AA465EFDD12D6AE",
                    "9EE522DC7538353A42F0966C9A979EB3",
                    "B9BFDB25B487F952EF8402AC8DCF22A7",
                    "3CE33DD113987DA6A4E666CD5E4FF8DC",
                    "5552331D7E4CF359543CDD9F0728310D",
                    "8B9F113B037517FBAC46DC9F6B01B3F7",
                    "60CB627BA7C26CCBCEE6A420F35A9B65",
                    "AC32ABD3C586C5FA35B4DAA452FF4045",
                    "9AEE1FF41CED65E050731BC3C45A266C"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 117,
                "giftCode": "GiftInfor_20230622092756400_2631",
                "giftName": "LOADTEST",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/0fa169acbf3bbe3dcb4b9d7eca9bb86c.png",
                "isInWishlist": false,
                "countGift": 3.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_54JKXX",
                    "LOADTEST_96X6FX",
                    "LOADTEST_4WBL0Z"
                ],
                "giftTransactionCodes": [
                    "C000DB4BEB8E2BDF4C6751C2C0B9B43B",
                    "3C69131A1E8D12BA317F4E798256B9AA",
                    "8CC14B9DA84C824679962F9D9482B02D"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 132,
                "giftCode": "GiftInfor_20230627041208329_1798",
                "giftName": "LOADTEST_ZZZ",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/5ebd49c7f22e726a025fca98e3a8e286.png",
                "isInWishlist": false,
                "countGift": 4.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_GVRU01OCR9",
                    "LOADTEST_1KBTFYHD0L",
                    "LOADTEST_YS3PMTSO9W",
                    "LOADTEST_ZFSNYNX9C4"
                ],
                "giftTransactionCodes": [
                    "AE188F78F1ED62303FA55AF6855CCE32",
                    "AB934212718B839DC5219A18273586D4",
                    "C668814AFA3BA212C1057F3AD1E7F052",
                    "C499576829C2FAAD0BE8AB9C3213A444"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 129,
                "giftCode": "GiftInfor_20230627035955665_1621",
                "giftName": "LOADTEST_FPT",
                "isUseOnline": true,
                "isUseOffline": false,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/f0b57362d85211c455b5a4ee0c4f5fa8.png",
                "isInWishlist": false,
                "countGift": 4.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_GSAEPKKIGB",
                    "LOADTEST_ZF5S1HT119",
                    "LOADTEST_7QW4VN3YSM",
                    "LOADTEST_8TDG804HQP"
                ],
                "giftTransactionCodes": [
                    "A7192FAE3EBE051DC371522B44952483",
                    "E37D93062E4ECBCA977AC51423DCEEBA",
                    "7DC9859AF7C8268AFF4FBC12C05F843F",
                    "44D8DD5B478499D1643C9E90E25862A5"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 123,
                "giftCode": "GiftInfor_20230626100613219_9205",
                "giftName": "LOADTEST_ABC",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/2b2d49e3150b76ef055e0ada6cc742f9.jpg",
                "isInWishlist": false,
                "countGift": 10.000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_FLP35D1B01",
                    "LOADTEST_LZ5WM8TDF6",
                    "LOADTEST_VEQU3K38W9",
                    "LOADTEST_Q4ZRC59N2D",
                    "LOADTEST_SAVGDJKO7N",
                    "LOADTEST_BJS2E0FAGD",
                    "LOADTEST_PI2I9Q7N4O",
                    "LOADTEST_NCPQLWH1RC",
                    "LOADTEST_YL98KCPIDV",
                    "LOADTEST_6TBII80OQ3"
                ],
                "giftTransactionCodes": [
                    "DCBF4E35D48211A3DBECCF8A463DC5A9",
                    "8859A94AC51BED8CE4D11EFE8F74FDCD",
                    "2E6B545648400C7C2C4D5AA6FE73E34A",
                    "0F1EC5D6F828995AB3981FD813B4FE65",
                    "C206C881C91CC46606B6150EFD8E8888",
                    "F44F87B954EFBF65BCBE832004C9DA57",
                    "72A5BD71CF8A8A4D2E35B99AE7E16385",
                    "4F39D6626A1A55B9E62430400D88F7FA",
                    "9B32962D4F80E9190DDA56BA9E955E7C",
                    "5F6E859BB9E60C7C3CB3EA7E5B40DC64"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 126,
                "giftCode": "GiftInfor_20230626101146665_2016",
                "giftName": "LOADTEST_789",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/5950f0787ca40d02337bb5855789d603.png",
                "isInWishlist": false,
                "countGift": 4.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_48EPJDX04Q",
                    "LOADTEST_YBWVO8GTU5",
                    "LOADTEST_FL4N3JN6XH",
                    "LOADTEST_7801UOLG3A"
                ],
                "giftTransactionCodes": [
                    "96684B029DBE7EF6C432B9F55944D034",
                    "EDC757BAA7D1412D8704EF3FF8FD4CCD",
                    "01DF674975C6065FE7FFDD8FCA045D07",
                    "DDFF2C1FF03BCCDCED6080BFF8D479C2"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 124,
                "giftCode": "GiftInfor_20230626100832203_7712",
                "giftName": "LOADTEST_XYZ",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/a2f5fd10cf23df6e163b6e6a5df28a02.jpg",
                "isInWishlist": false,
                "countGift": 3.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_SAVGDJKO7N",
                    "LOADTEST_BJS2E0FAGD",
                    "LOADTEST_PM0MGGQG49"
                ],
                "giftTransactionCodes": [
                    "3DAC2E850ED4C883019EA536CED921C7",
                    "EA0595A236900BCBE784C42EBBD788AF",
                    "39DECDD6928DA30531FE4A3394CC5BF1"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 121,
                "giftCode": "GiftInfor_20230626095005164_4462",
                "giftName": "LOADTEST_OAO",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/1c411f9b7cb0f597e2df510ea152cd4e.jpg",
                "isInWishlist": false,
                "countGift": 2.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_B902KW2BDR",
                    "LOADTEST_PI2I9Q7N4O"
                ],
                "giftTransactionCodes": [
                    "6F120E061DAB25E8E2B6F73FD990829F",
                    "496051C3A00597165E7DD9E01DAAE1BF"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 130,
                "giftCode": "GiftInfor_20230627040128500_3394",
                "giftName": "LOADTEST_HCM",
                "isUseOnline": true,
                "isUseOffline": false,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/7bcfc570b419da9d2d2906ad76d26c85.jpg",
                "isInWishlist": false,
                "countGift": 2.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_13FALPGTOY",
                    "LOADTEST_0NUN7CYELC"
                ],
                "giftTransactionCodes": [
                    "638DBDB80AE76B0E333A998A8E453CB6",
                    "2BEB8F2219FF5E6E86A81A249A4D1CDC"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 118,
                "giftCode": "GiftInfor_20230626093107966_2185",
                "giftName": "LOADTEST_TIENNHM",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/83004f94ed7938d8fe90688ef468c51d.png",
                "isInWishlist": false,
                "countGift": 2.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_LZ5WM8TDF6",
                    "LOADTEST_059EQZBVIA"
                ],
                "giftTransactionCodes": [
                    "36A9060812B50DC06A38DAFDF9B89991",
                    "F65D8D2206D3C2895E3F4F23CBF7FA0C"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 125,
                "giftCode": "GiftInfor_20230626101019818_0755",
                "giftName": "LOADTEST_456",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/345e63f287894cfad201b72208b45fae.jpg",
                "isInWishlist": false,
                "countGift": 3.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_GDC0T98ITF",
                    "LOADTEST_LOWSJKK65L",
                    "LOADTEST_NPPO4VLV2L"
                ],
                "giftTransactionCodes": [
                    "9D8A43D73050918AA4D51D5342CF044F",
                    "145D28CFC5915CD1C88B359297AAA2F4",
                    "2DE7AD79EB3BFAD8BED70BF23B71ED6C"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 128,
                "giftCode": "GiftInfor_20230627035604613_0680",
                "giftName": "LOADTEST_UTOP",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/e25ada0585b09c873c83b0011a5187d9.jpg",
                "isInWishlist": false,
                "countGift": 2.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_48EPJDX04Q",
                    "LOADTEST_NPPO4VLV2L"
                ],
                "giftTransactionCodes": [
                    "C7ED9C93E6A4A8A027315DDAF6A706DD",
                    "F11C067465F9C507899A002DBD9FBB05"
                ]
            },
            {
                "brandId": 47,
                "brandName": "LOADTEST",
                "coin": 1.0000000000000000000000000000,
                "giftId": 122,
                "giftCode": "GiftInfor_20230626100125649_1594",
                "giftName": "LOADTEST_123",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/3ac798fc0d4cefbbd9a9acfef2f2d4f7.png",
                "isInWishlist": false,
                "countGift": 2.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "LOADTEST_LZ5WM8TDF6",
                    "LOADTEST_059EQZBVIA"
                ],
                "giftTransactionCodes": [
                    "573C389A047A3F905E220C19BEC00AF0",
                    "5402C4CB67067387798B731082B48866"
                ]
            },
            {
                "brandId": 2,
                "brandName": "PEDRO",
                "coin": 1.0000000000000000000000000000,
                "giftId": 115,
                "giftCode": "GiftInfor_20230620082214866_9391",
                "giftName": "Phiếu giảm giá thời trang",
                "isUseOnline": true,
                "isUseOffline": true,
                "expiredDate": "2023-06-30T16:59:59Z",
                "imageLink": "https://xxx.com/upload-gift/4b60e972c5c0f0011dad9e6b8e6477e0.jpg",
                "isInWishlist": false,
                "countGift": 1.0000000000000000000000000000,
                "isEGift": true,
                "eGiftCodes": [
                    "EgiftCode009"
                ],
                "giftTransactionCodes": [
                    "DC5382BCFD9FE12A28929F82530C3377"
                ]
            }
        ]
    },
    "targetUrl": null,
    "success": true,
    "error": null,
    "unAuthorizedRequest": false,
    "__abp": true
}

var OwnerCode = "XXX164010780";
var EGiftStatus = "R";
var data = response.result.items;
var result = []

for (var i = 0; i < data.length; i++) {
    var items = data[i].eGiftCodes;

    var egifts = items.map((egiftCode) => {
        return {
            "OwnerCode": OwnerCode,
            "GiftCode": data[i].giftCode,
            "EGiftCode": egiftCode,
            "EGiftStatus": EGiftStatus,
            "Producer": data[i].giftName
        }
    })
    result = result.concat(egifts);
}

JSON.stringify(result)