/**
 * @name CreateRedeemTransactionVer2
 * @summary Create Redeem Transaction Ver2
 * @description docker-compose run k6 run /scripts//Gift/CreateRedeemTransactionVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { scenario } from 'k6/execution';
import { Rate } from "k6/metrics";
import { CONTENT_TYPE, MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";
import { UtcNow, DateTimeNow } from "../utils/DateTimeHelper.js";

const MEMBERS = new SharedArray("members", function () {
  return JSON.parse(open("../data/accounts.json"));
});
const GIFTS = new SharedArray("gifts", function () {
  return JSON.parse(open("../data/gifts.json"));
});

const dataMemberCodes = open("../data/MemberCodes_50.txt");
var MEMBER_CODES = dataMemberCodes.split("\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

var VUS = 100;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]

  vus: VUS,
  duration: "5m",
  rps: VUS

  // scenarios: {
  //   'CreateRedeemTransactionVer2': {
  //     executor: 'shared-iterations',
  //     vus: VUS,
  //     iterations: MEMBER_CODES.length,
  //     maxDuration: '5m',
  //   },
  // },
};

const API = "GiftTransactions/CreateRedeemTransactionVer2";
// var url = `${MSL_MOBILE_URL}/api/${API}`;
var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;

var params = {
  headers: {
    "accept": "text/plain",
    "Content-Type": CONTENT_TYPE.JSON,
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUwIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFrY3N1cHBvcnQiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IklaQlJLNlJTUlJNN1Q3UU5GM083V0MyTlRONkRIV0FJIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIjc1YmE2YzdlMTUwMzQ3Y2JhMTZhYTgxZDY2MTVhZTc0IiwiVXNlciIsIkFkbWluIl0sImh0dHA6Ly93d3cuYXNwbmV0Ym9pbGVycGxhdGUuY29tL2lkZW50aXR5L2NsYWltcy90ZW5hbnRJZCI6IjMiLCJzdWIiOiI1MCIsImp0aSI6ImFhZTZhYzkwLTEzMzQtNGZmNC04MTQxLTU4YjdmYTM4MjAwZSIsImlhdCI6MTY4ODYwOTUyNywidG9rZW5fdmFsaWRpdHlfa2V5IjoiM2U5ZDkwMWQtYTM4NS00YjUyLWIxMDEtNDk1YzdjNjg5YjUzIiwidXNlcl9pZGVudGlmaWVyIjoiNTBAMyIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjg4NjA5NTI3LCJleHAiOjE2ODg2OTU5MjcsImlzcyI6IlNhYVMiLCJhdWQiOiJTYWFTIn0.5YZ814LROoSNFdThjhJKta9DuoEsYmVL6VhZFsimR5M"
  }
}

export let errorRate = new Rate("errors");

export default function (data) {

  var offset = __ITER * VUS + __VU;
  // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
  var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];
  // var memberCode = MEMBER_CODES[scenario.iterationInTest];
  var gift = GIFTS[(__ITER * VUS + __VU) % GIFTS.length];
  var quantity = 1;
  var payload = JSON.stringify(
    {
      "memberCode": memberCode,
      "giftCode": gift.Code,
      "quantity": quantity,
      "totalAmount": gift.RequiredCoin * quantity, // Required coin
      "date": `${UtcNow()}`,
      "description": "LOADTEST",
      "transactionCode": `${gift.Producer}_${DateTimeNow()}_${offset}`
    }
  );

  let res = http.post(`${url}`, payload, params);

  if (!res.body || !res.body.includes('"success":true')) {
    console.log(JSON.stringify(res));
    console.log(payload);
  }

  var success = check(res, {
    // "log": r => console.log(JSON.stringify(r)),
    [`${API} is status 200`]: (r) => res.status === 200,
    [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
  });

  if (!success) {
    errorRate.add(1);
  }
};