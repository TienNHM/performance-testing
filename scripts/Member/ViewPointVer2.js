/**
 * @name ViewPointVer2
 * @summary Member ViewPoint Ver2
 * @description docker-compose run k6 run /scripts//Member/ViewPointVer2.js
 */

import { check } from "k6";
import http from "k6/http";
import { SharedArray } from "k6/data";
import { Rate } from "k6/metrics";
import { MSL_LOYALTY_URL, MSL_MOBILE_URL } from "../utils/Constants.js";
import { ConvertURLSearchParams } from "../utils/Common.js";

const MEMBERS = new SharedArray("members", function () {
    return JSON.parse(open("../data/accounts.json"));
});

const dataMemberCodes = open("../data/MemberCodes.txt");
var MEMBER_CODES = dataMemberCodes.split("\r\n");
MEMBER_CODES = [...new Set(MEMBER_CODES)];

const VUS = 200;
export let options = {
  // stages: [
  //   {duration: "1m", target: 400},
  //   {duration: "5m", target: 400},
  // ]
  vus: VUS,
  duration: "5m",
//   rps: VUS
};

const API = "Member/ViewPointVer2";
var url = `${MSL_MOBILE_URL}/api/${API}`;
// var url = `${MSL_LOYALTY_URL}/api/services/app/${API}`;
var params = {
    headers: {
        "accept": "text/plain",
        "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImY5N2U3ZWVlY2YwMWM4MDhiZjRhYjkzOTczNDBiZmIyOTgyZTg0NzUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbWFpc29uLXByb2QiLCJhdWQiOiJtYWlzb24tcHJvZCIsImF1dGhfdGltZSI6MTY4ODExMDgxMiwidXNlcl9pZCI6Ild6aDZ5MFFPcExOMG9WRmh0b09LaVoxOGd5czEiLCJzdWIiOiJXemg2eTBRT3BMTjBvVkZodG9PS2laMThneXMxIiwiaWF0IjoxNjg4MTEwODEyLCJleHAiOjE2ODgxMTQ0MTIsInBob25lX251bWJlciI6Iis4NDM4ODk2MzM0NSIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsicGhvbmUiOlsiKzg0Mzg4OTYzMzQ1Il19LCJzaWduX2luX3Byb3ZpZGVyIjoicGhvbmUifX0.Yrwsk9ljAARPo49RiodxzmlCd_Yn5182VQL7ml4jQmsbVvfxFI0x9CZM9dJ_W3ivO-Nq2xFL37kKP_snnAojIFcCDHbmgjfOGtP3wpxLdixeP-T0ZHxzKLIkbdnyqKJ5XMbVxHitwMXDEjbuej1pkhcOzrAymNOOQvxNvVOBSvwJleRRP6XwLc9RiigkkY3ryQBnN6c6I7OM8eLyWoKxxK2UKFmQfzG1RK6kHTC-knmM6NPsFFJbFYhQuE23l35JtGyNfN6CnJOb6lhyXFTSsAGaQmPppzvEbgGWoljZRd7Zlo5QBj_lmcyC9gvQ4O86SgQSkKaUqtr3GZuSD-Ix8g"
    }
};

export let errorRate = new Rate("errors");

export default function (data) {

    // var member = MEMBERS[(__ITER * VUS + __VU) % MEMBERS.length];
    var memberCode = MEMBER_CODES[(__ITER * VUS + __VU) % MEMBER_CODES.length];
    const urlParams = ConvertURLSearchParams({
        "Code": memberCode,
    });
    let res = http.get(`${url}?${urlParams}`, params);

    if (!res.body || !res.body.includes('"success":true')) {
        console.log(JSON.stringify(res));
    }

    var success = check(res, {
       // "log": res => console.log(JSON.stringify(res)),
        [`${API} is status 200`]: (r) => res.status === 200,
        [`${API} body contains success true`]: (r) => res.body.includes('"success":true')
    });

    if (!success) {
        errorRate.add(1);
    }
};